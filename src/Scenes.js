import React from 'react';
import {
    // Actions,
    Scene,
    Router,
    Stack,
    Drawer,
    // Modal,
} from 'react-native-router-flux';
// import { StackViewStyleInterpolator } from 'react-navigation-stack';
// import Trending from './screens/Trending';
// import ErrorModal from './components/modal/ErrorModal';
// import DrawerContent from './components/drawer/DrawerContent';

import IntroPage from './components/IntroPage';

import Login from './components/Login';
import ForgetPassword from './components/ForgetPassword';

import SignUp from './components/SignUp';
import VerifySignUp from './components/SignUp/VerifySignUp';

import HomePage from './components/HomePage';
import ProfileSetting from './components/ProfileSetting';

import CheckPrice from './components/CheckPrice';
import CheckNearestDriver from './components/CheckNearestDriver';
import CheckDeliveryTime from './components/CheckDeliveryTime';

import DrawerContent from './components/DrawerContent';

// const transitionConfig = () => ({
//   screenInterpolator: StackViewStyleInterpolator.forFadeFromBottomAndroid,
// });

const Scenes = () => (
    <Router>
        {/* <Modal hideNavBar> */}
        <Scene key="root" hideNavBar>
            {/* <Scene key="loginSignUp" hideNavBar> */}
            <Scene key="IntroPage" component={IntroPage} hideNavBar />

            <Scene key="Login" component={Login} hideNavBar />
            <Scene key="ForgetPassword" component={ForgetPassword} hideNavBar />

            <Scene key="SignUp" component={SignUp} hideNavBar />
            <Scene key="VerifySignUp" component={VerifySignUp} hideNavBar />
            {/* </Scene> */}

            <Scene
                key="CheckNearestDriver"
                component={CheckNearestDriver}
                hideNavBar
            />
            <Scene key="CheckPrice" component={CheckPrice} hideNavBar />
            <Scene key="CheckDeliveryTime" component={CheckDeliveryTime} hideNavBar />

            <Drawer
                hideNavBar
                key="drawer"
                contentComponent={DrawerContent}
                drawerWidth={300}>
                {/* <Stack key="Home" hideNavBar> */}
                <Scene key="HomePage" component={HomePage} hideNavBar />
                <Scene key="ProfileSetting" component={ProfileSetting} hideNavBar />
                {/* </Stack> */}
            </Drawer>
        </Scene>
        {/* <Scene key='error' component={ErrorModal} /> */}
        {/* </Modal> */}
    </Router>
);

export default Scenes;
