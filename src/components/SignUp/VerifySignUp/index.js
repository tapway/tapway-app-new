/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import styles from './styles';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Card,
  CardItem,
  Input,
  Button,
  Item,
} from 'native-base';
import Image from 'react-native-remote-svg';
import { Actions } from 'react-native-router-flux';
import Modal from 'react-native-modal';

export class VerifySignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVerified: false,
    };
  }

  render() {
    const { isVerified } = this.state;
    return (
      <Container style={styles.container}>
        <Header transparent androidStatusBarColor={'#0003'}>
          <Left style={{ paddingHorizontal: 5 }}>
            <Button transparent onPress={() => Actions.Login()}>
              <Icon name={'arrow-back'} style={styles.closeIcon} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content>
          <Card transparent>
            <CardItem>
              <Text style={styles.title}>
                Enter 4 digit code sent to the email address provided
              </Text>
            </CardItem>
            <CardItem>
              <Item regular style={styles.emailContainer}>
                <Input
                  placeholder={'5'}
                  placeholderTextColor={'#0309'}
                  style={styles.emailInput}
                />
                <Input
                  placeholder={'7'}
                  placeholderTextColor={'#0309'}
                  style={styles.emailInput}
                />
                <Input
                  placeholder={'7'}
                  placeholderTextColor={'#0309'}
                  style={styles.emailInput}
                />
                <Input
                  placeholder={'7'}
                  placeholderTextColor={'#0309'}
                  style={styles.emailInput}
                />
              </Item>
            </CardItem>

            <View style={{ padding: 16 }}>
              <Button
                block
                full
                onPress={() => this.setState({ isVerified: true })}
                style={styles.submitButton}>
                <Text style={styles.submitText}>VERIFY</Text>
              </Button>
            </View>

            <View style={{ marginVertical: 10 }}>
              <Text
                style={{
                  color: '#1B2E5A',
                  textAlign: 'center',
                  fontFamily: 'Nunito-Regular',
                  fontSize: 16,
                }}>
                Didn’t recieve a verification code?
              </Text>
              <TouchableOpacity onPress={() => Actions.SignUp()}>
                <Text
                  style={{
                    color: '#3FAF5D',
                    textAlign: 'center',
                    fontWeight: '600',
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 14,
                  }}>
                  Resend code
                </Text>
              </TouchableOpacity>
            </View>
          </Card>
        </Content>
        <Modal
          isVisible={isVerified}
          coverScreen={true}
          style={{ backgroundColor: '#FFFFFF' }}
          backdropColor={'#FFFFFF'}>
          <View style={styles.modalContainer}>
            <View style={{ alignSelf: 'center' }}>
              <Image
                style={styles.verifyImageSize}
                source={require('../../../assets/icons/success.svg')}
              />
            </View>
            <Card transparent>
              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalTitle}>Signup Successful</Text>
              </CardItem>

              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalSubtitle}>
                  {
                    'You have successfully created your account, now get started to start using Tapway'
                  }
                </Text>
              </CardItem>

              <View style={{ padding: 16 }}>
                <Button
                  block
                  full
                  onPress={() =>
                    this.setState({ isVerified: false }, () =>
                      Actions.ProfileSetting(),
                    )
                  }
                  style={styles.modalButton}>
                  <Text style={styles.modalButtonText}>GET STARTED</Text>
                </Button>
              </View>
            </Card>
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VerifySignUp);
