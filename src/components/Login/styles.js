import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1891D0',
  },
  white: {
    color: '#FFFFFF',
  },
  welcomeHd: {
    fontWeight: '600',
    color: '#FFFFFF',
    fontSize: 30,
    fontFamily: 'Nunito-Bold',
    width: '70%',
  },
  inputContainer: {
    marginVertical: 5,
  },
  inputLabel: {
    fontSize: 18,
    fontWeight: '100',
    color: '#FFFFFF',
    opacity: 0.7,
  },
  inputItem: {
    borderColor: '#FFF9',
    color: '#FFFFFF',
    borderWidth: 1,
    fontSize: 22,
    marginTop: 3,
    paddingHorizontal: 10,
    height: 45,
    fontFamily: 'Nunito-Regular',
  },
  inputText: {
    color: '#FFFFFF',
    opacity: 0.9,
  },
  submitBtn: {
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    marginTop: 40,
    marginBottom: 30,
  },
  btnText: {
    color: '#000000',
    fontSize: 18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '400',
    letterSpacing: 0.5,
  },
  signUp: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerMenu: {
    // bottom: 20,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  actionContainer: {
    flex: 1,
    marginTop: 30,
  },
  actionTitle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '200',
    fontFamily: 'Nunito-Regular',
  },
  actionColView: {
    alignSelf: 'center',
    backgroundColor: '#FFF5',
    padding: 20,
    borderRadius: 50,
  },
  actionImageSize: { width: 25, height: 25 },
  actionText: {
    color: '#FFFFFF',
    fontSize: 12,
    fontFamily: 'Nunito-Regular',
    textAlign: 'center',
  },
});

export default styles;
