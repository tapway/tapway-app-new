/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Button,
} from 'native-base';
import { Actions } from 'react-native-router-flux';

export class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <Container>
        <Header transparent androidStatusBarColor={'#0001'}>
          <Left>
            <Button transparent onPress={() => Actions.drawerOpen()}>
              <Icon
                style={{ paddingHorizontal: 10, color: '#000000' }}
                name={'menu'}
              />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content contentContainerStyle={{ padding: 10 }}>
          <View>
            <Text>Home Page</Text>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);
